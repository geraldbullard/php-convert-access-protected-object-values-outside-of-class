<?php
/*
Created to access priivate and protected values in an object without having to modify or extend the parent class.
TY: https://stackoverflow.com/users/1202145/bhargav-nanekalva for the main function.
*/

function dismount($object) {
    $reflectionClass = new ReflectionClass(get_class($object));
    $array = array();
    foreach ($reflectionClass->getProperties() as $property) {
        $property->setAccessible(true);
        $array[$property->getName()] = $property->getValue($object);
        $property->setAccessible(false);
    }
    return $array;
}

/* My use case */
$participant_object = json_decode(json_encode(dismount($currentParticipant)))->object;

echo $participant_object->name; // "Jon Doe" on the page
